function InitVars(pid, user)
    let g:Rc_fifo='/tmp/'.a:user.'/rc/rc-'.a:pid
    let g:Rc_cmd ='/tmp/'.a:user.'/rc/rc-'.a:pid.'-cmd'
    let g:Rc_rdy ='/tmp/'.a:user.'/rc/rc-'.a:pid.'-rdy'
    let g:Rc_pwd ='/tmp/'.a:user.'/rc/rc-'.a:pid.'-pwd'
    let g:Rc_vim_term ='/tmp/'.a:user.'/rc/rc-'.a:pid.'-vim-term'
    let g:Rc_dict = '/tmp/'.a:user.'/rc/rc-cmd-dict'
    let &dictionary .= ','.g:Rc_dict
    set iskeyword+=\-
endfunction

function GetVisualSelection()
    let reg_save = getreg('"')
    let regtype_save = getregtype('"')
    let cb_save = &clipboard
    set clipboard&
    normal! ""gvy
    let selection = getreg('"')
    call setreg('"', reg_save, regtype_save)
    let &clipboard = cb_save
    return selection
endfunction

function GetExpr(mode)
    if (a:mode ==# "v")
        let expr = GetVisualSelection()
        if len(expr) != 0
            return split(expr, '\n')
        end
    end
    if (a:mode ==# "g")
        return getline(1, '$')
    end
    let expr = [getline('.')]
    return expr
endfunction

function ExecRcCmd(cmd)
    call writefile(a:cmd, g:Rc_cmd)
    exe 'silent !echo rdy > ' g:Rc_fifo
endfunction

function SyncWd()
    exe 'silent !cat ' g:Rc_rdy
    let g:Vim_wd = readfile(g:Rc_pwd, "b", 1)[0]
    exe 'silent lcd' g:Vim_wd
endfunction

function TryExecRcCmdSyncWd(cmd)
    if (filewritable(g:Rc_fifo))
        call ExecRcCmd(a:cmd)
        call SyncWd()
        redraw!
    endif
endfunction

function InitWd()
    call TryExecRcCmdSyncWd(['true'])
endfunction

function TermRc()
    if (filewritable(g:Rc_fifo))
        exe 'silent !touch ' g:Rc_vim_term
        call ExecRcCmd(['exit 0'])
    else
        exe 'silent !rm ' g:Rc_pwd
    endif
endfunction

function ExecuteLine(mode)
    call TryExecRcCmdSyncWd(GetExpr(a:mode))
endfunction

function NewTab()
    call TryExecRcCmdSyncWd(['if (! ~ $#TMUX 0) {tmux new-window}'])
endfunction

function BindRcKeys()
    nnoremap <enter> :call ExecuteLine('n')<enter>
    vnoremap <enter> :<C-U>call ExecuteLine('v')<enter>
    call ToggleInsExec()
    nmap t :silent call NewTab()<enter>
    nmap <C-t> t
    imap <C-t> <esc>ti
endfunction

function ScrollRc()
    silent !tmux select-pane -t +
    silent !tmux copy-mode
    silent !tmux send-keys H
    redraw!
endfunction

function ToggleInsExec()
    if (g:InsExec)
        iunmap <enter>
    else
        inoremap <enter> <enter><esc>k:call ExecuteLine('i')<enter>ji
    endif
    let g:InsExec = !g:InsExec
endfunction

let g:InsExec = 0
call BindRcKeys()
imap <F1> <C-X><C-F>
nmap <F2> :silent call ScrollRc()<enter>
imap <F2> <esc><F2>li
imap <F3> <C-X><C-K>
nmap <F4> :silent call ToggleInsExec()<enter>
imap <F4> <esc><F4>li
nmap q ZQ
exe "au VimLeave * :call TermRc()"
