all:

DESTDIR ?= $(HOME)
PREFIX ?= /bin/rc
MANPREFIX ?= /man

install:
	mkdir -p $(DESTDIR)$(PREFIX)
	cp -f ias $(DESTDIR)$(PREFIX)/ias
	cp -f ias_cmd $(DESTDIR)$(PREFIX)/ias_cmd
	cp -f caesium $(DESTDIR)$(PREFIX)/caesium
	cp -f caesium-glue.rc $(DESTDIR)$(PREFIX)/caesium-glue.rc
	cp -f caesium-rc.rc $(DESTDIR)$(PREFIX)/caesium-rc.rc
	cp -f caesium.vim $(DESTDIR)$(PREFIX)/caesium.vim
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	cp -f caesium.1 $(DESTDIR)$(MANPREFIX)/man1/caesium.1

.PHONY: install
